/*
 * grep.h
 *
 *      Author: zephyr
 */

#ifndef GREP_H_
#define GREP_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int bool;
#define TRUE 1
#define FALSE 0
#define MAX_LINE_SIZE 255

extern bool findWord(FILE *f, char *word);


#endif /* GREP_H_ */
