///*
// * grep.c
// *
// *      Author: zephyr
// */
//
//
//#include "grepSequential.h"
//
//int main(int argc, char* argv[])
//{
//	// Get the user input
//	char *fileName = argv[1];
//	char *word = argv[2];
//
//	// Check the user input
//	if(fileName == NULL || word == NULL)
//	{
//		printf("Please input a file and a word.\n");
//		return 0;
//	}
//
//	// Open the file
//	printf("Opening File: %s...\n",fileName);
//	FILE *f;
//	f = fopen(fileName, "r");
//
//	// Check file open
//	if(f == NULL)
//	{
//		printf("Fail to open File: %s\n", fileName);
//		return 0;
//	}
//
//	// Find the word
//	printf("Finding word: %s...\n", word);
//	if(findWord(f, word))
//		printf("Done\n");
//	else
//		printf("No matching found\n");
//
//	//Close up
//	fclose(f);
//	printf("Program exit\n");
//
//	return 0;
//}
//
//bool
//findWord(FILE *f, char *word)
//{
//	bool isFound = TRUE;
//	char line[MAX_LINE_SIZE+1];
//
//	while((fgets(line, MAX_LINE_SIZE, f)) != NULL)
//	{
//		if(strstr(line, word) != NULL)
//		{
//			printf("%s\n", line);
//			isFound = TRUE;
//		}
//	}
//	return isFound;
//}
//
