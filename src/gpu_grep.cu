/*
 * gpu_grep.cu
 *
 * 	 Author: Ying Ni(yni6@hawk.iit.edu)
 * 	 Description:
 *
 */

 #include <stdio.h>
 #include <string.h>
 #include <stdlib.h>

#define SUCCESS 0
#define FAIL -1
#define BUFFER_SIZE 10240
#define NUM_THREADS 65536
 
#define CHECK_ERR(x) \
	if (x != cudaSuccess) { \
		fprintf(stderr, "%s in %s at line %d\n", cudaGetErrorString(x), __FILE__, __LINE__); \
    	exit(FAIL); \
	}

int grep(char *word, char *filename);

int main(int argc, char *argv[]) {
	if (argc < 3) {
		fprintf(stderr, "Missing argument(s).\n");
		return FAIL;
	}

	char *word = argv[1];
	int i;
	for (i = 2; i < argc; i++) {
		char *filename = argv[i];
		if (grep(word, filename) != SUCCESS) {
			fprintf(stderr, "Grep failed.\n");
			return FAIL;
		}
	}
	return SUCCESS;
}

__global__ void searchInLine(char *d_Words, char *d_Lines, int numLines) {
	int tid = blockDim.x * blockIdx.x + threadIdx.x;
	if (tid >= numLines) {
		return;
	}
	char *c = d_Lines + sizeof(char) * BUFFER_SIZE * tid;
	// read each character in the line
	for (; *c != '\n'; c++) {
		// if the character matches the first character of the word
		if (*c == d_Words[0]) {
			char *key = d_Words;
			char *tmp = c;
			// compare the following characters in the line with the ones in the word
			for (; *key != '\0' && *tmp != '\n' && *key == *tmp; key++, tmp++)
				;
			// if the following characters all matches the ones in the word, found
			if (*key == '\0') {
				printf("%s", d_Lines + sizeof(char) * BUFFER_SIZE * tid);
				return;
			}
		}
	}
}

int grep(char *words, char *filename) {
	cudaError_t err;
	char buf[BUFFER_SIZE];
	
	FILE *file = fopen(filename, "r");
	if (!file) {
		fprintf(stderr, "Cannot open file '%s'.\n", filename);
		return FAIL;
	}
	
	char *d_Lines;
	char *d_Words;

	size_t size = strlen(words) + 1;
	err = cudaMalloc(&d_Words, sizeof(char) * size);
  	CHECK_ERR(err);
  	err = cudaMemcpy(d_Words, words, sizeof(char) * size, cudaMemcpyHostToDevice);
  	CHECK_ERR(err);
  	
  	err = cudaMalloc(&d_Lines, sizeof(char) * BUFFER_SIZE * NUM_THREADS);
  	CHECK_ERR(err);
  	
  	int num = 0;
	while (fgets(buf, sizeof(buf), file)) {
  		err = cudaMemcpy(d_Lines + sizeof(char) * BUFFER_SIZE * num, buf, sizeof(char) * BUFFER_SIZE, cudaMemcpyHostToDevice);
  		CHECK_ERR(err);
		if (num < NUM_THREADS - 1) {
  			num++;
		} else {
			dim3 dimGrid(ceil(NUM_THREADS / 1024.0), 1);
			dim3 dimBlock(1024, 1, 1);
			searchInLine<<<dimGrid, dimBlock>>>(d_Words, d_Lines, NUM_THREADS);
			cudaThreadSynchronize();

			num = 0;
		}
	}
	
	dim3 dimGrid(ceil(num / 1024.0), 1);
	dim3 dimBlock(1024, 1, 1);
	searchInLine<<<dimGrid, dimBlock>>>(d_Words, d_Lines, num);
	cudaThreadSynchronize();
		
	cudaFree(d_Lines);
	cudaFree(d_Words);
	
	fclose(file);
	return SUCCESS;
}