/*
 * grep.h
 *
 *      Author: zephyr
 */

#ifndef GREP_H_
#define GREP_H_

#include <stdio.h>
#include <stdlib.h>

#ifndef CHECK_ERR
	#define CHECK_ERR(x)                                    \
	  if (x != cudaSuccess) {                               \
		fprintf(stderr,"%s in %s at line %d\n",             \
			cudaGetErrorString(err),__FILE__,__LINE__);		\
		exit(-1);											\
	  }
#endif

#define TRUE 1
#define FALSE 0
#define LINE_SIZE 1024
#define LINE_COUNT 1024 //how many lines to bundle together as a block

__global__ void grep(char *buffer_d, char *word, int lineCount, int lineSize);

#endif /* GREP_H_ */
