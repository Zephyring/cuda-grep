/*
 * grep.cu
 *
 *      Author: zephyr
 */

#include "grepCuda.h"

static void readFileOnHost(char *buffer_h, FILE **f);
static char *mallocBufferOnDevice();
static void copyBufferToDevice(char *buffer_h, char *buffer_d);
static char *mallocAndCopyToDevice(char *word, int length);
static void freeOnDevice(void *a_d);
static void freeOnHost(void *a_h);

int
main(int argc, char* argv[])
{
	// Get the user input
	char *fileName = argv[1];
	char *word = argv[2];

	// Check the user input
	if(fileName == NULL || word == NULL)
	{
		printf("Please input a file and a word.\n");
		return 0;
	}

	// Open the file
	printf("Opening File: %s...\n",fileName);
	FILE *f;
	f = fopen(fileName, "r");

	// Check file open
	if(f == NULL)
	{
		printf("Fail to open File: %s\n", fileName);
		return 0;
	}
	
	// Malloc space and copy word to device
	char *word_d = mallocAndCopyToDevice(word, strlen(word));

	// Malloc at most LINE_SIZE*LINE_COUNT byte space for file buffer on host
	// Read LINE_COUNT lines of file into buffer on host
	char *buffer_h = (char*)malloc(sizeof(char)*LINE_SIZE*LINE_COUNT);

	// Malloc buffer on device
	char *buffer_d = mallocBufferOnDevice();
	
	while(!feof(f))
	{
		readFileOnHost(buffer_h, &f);
		copyBufferToDevice(buffer_h, buffer_d);
		// Call kernel function
		grep<<<ceil(LINE_COUNT/1024.0), 1024>>>(buffer_d, word_d, LINE_COUNT, LINE_SIZE);
	}

	//Close up
	freeOnDevice(buffer_d);
	freeOnDevice(word_d);
	freeOnHost(buffer_h);
	fclose(f);
	printf("Done\n");

	return 0;
}

__global__ void
grep(char *buffer_d, char *word, int lineCount, int lineSize)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;

	if (i < lineCount)
	{
		// pattern matching begin
		char *p1 = buffer_d+i*lineSize;
		while(*p1)
		{
			char *begin = p1, *p2 = word;
			while(*p1 && *p2 && *p1 == *p2)
			{
				p1++;
				p2++;
			}
			if(!*p2)
			{
				printf("%s",buffer_d+i*lineSize);
			}
			p1 = begin + 1;
		}
		// pattern matching end
	}
}

static void
readFileOnHost(char *buffer_h, FILE **f)
{
	char line[LINE_SIZE];
	int i;
	for(i = 0; i < LINE_COUNT; i++)
	{
		if(fgets(line, LINE_SIZE, *f) != NULL)
			memcpy(buffer_h+i*LINE_SIZE, line, LINE_SIZE);
		else
			break;
	}
}
static char *
mallocBufferOnDevice()
{
	cudaError_t err;
	char *buffer_d;

	// allocate memory on device
	err = cudaMalloc((void **)&buffer_d, sizeof(char)*LINE_SIZE*LINE_COUNT);
	CHECK_ERR(err);

	return buffer_d;
}

static void
copyBufferToDevice(char *buffer_h, char *buffer_d)
{
	cudaError_t err;
	// copy content to device
	err = cudaMemcpy(buffer_d, buffer_h, sizeof(char)*LINE_SIZE*LINE_COUNT, cudaMemcpyHostToDevice);
	CHECK_ERR(err);
}

static char *
mallocAndCopyToDevice(char *word, int length)
{
	cudaError_t err;
	char *word_d;

	// allocate memory on device
	err = cudaMalloc((void **)&word_d, sizeof(char)*length);
	CHECK_ERR(err);

	// copy content to device
	err = cudaMemcpy(word_d, word, sizeof(char)*length, cudaMemcpyHostToDevice);
	CHECK_ERR(err);
	return word_d;
}


static void
freeOnDevice(void *a_d)
{
	cudaError_t err;

	err = cudaFree(a_d);
	CHECK_ERR(err);
}

static void
freeOnHost(void *a_h)
{
	free(a_h);
}

